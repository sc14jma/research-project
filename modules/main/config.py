import pickle
"""
Parameters for main.py are altered here.
"""

#Frame parameters
FRAME_WIDTH = 10

width = 400
height = (width * 9)/16

# Enable or disable video during processing.
draw_video = False

#Erosion and Dilation values for noise removal
er_w = 7
er_h = 7

di_w = 15
di_h = 20

#MOG Erosion and dilation values
mog_er_w = 8
mog_er_h = 8

mog_di_w = 20
mog_di_h = 30

# moments
with open("points.txt","r") as f:
	points = pickle.load(f)

"""
These parameters are used to configure blobs.py
"""
# Configuration constant
BLOB_LIFE = 20              # life of blob in frames, if not seen
EDGE_THRESHOLD = 10         # border of image, in pixels, which is regarded as out-of-frame
DISTANCE_THRESHOLD = 60     # distance threshold, in pixels. If blob is further than this from previous position, update is ignored
MOVE_LIMIT = 70              # maximum velocity of the blob. If outside this limit, velocity is disabled
MATCH_DISTANCE = 40         # maximum distance between blobs in the Hungarian algorithm matching step
