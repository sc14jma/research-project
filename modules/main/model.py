from sklearn import tree, preprocessing
import numpy as np
import pandas as pd
import pydotplus


file_name = "../../samples/data.csv"

data = np.loadtxt(file_name,skiprows=1,dtype=str,delimiter=',')




clf = tree.DecisionTreeClassifier()
le = preprocessing.LabelEncoder()

data  = le.fit_transform(data)

X = data[:,0:3]
Y = data[:,3]

print X,Y

clf = clf.fit(X,Y)

dot_data = tree.export_graphviz(clf, out_file=None)
graph = pydotplus.graph_from_dot_data(dot_data)
graph.write_pdf("tree.pdf")