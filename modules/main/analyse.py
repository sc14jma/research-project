from __future__ import print_function
import numpy as np
import itertools
import config
import pylab
import scipy.signal, scipy.interpolate
import sys, getopt

w,h = config.width, config.height
border = config.EDGE_THRESHOLD
table_x, table_y = 315, 200

def all_tracks(tracks):
	valid_tracks = []

	for id,t in tracks:
		pts = np.array(list(t))
		invalid = False

		if not invalid:
			pts[:,1] = np.clip(pts[:,1],0,w)
			pts[:,2] = np.clip(pts[:,2],0,h)
			valid_tracks.append(pts)
	return valid_tracks


def filtered_tracks(traces):
	valid_tracks = []
	for id,t in traces:
		pts = np.array(list(t))
		invalid = False

		# bad starting point
		if (pts[0,1]>border and pts[0,1]<w-border) and (pts[0,2]>border and pts[0,2]<h-border):
			invalid = True


		fit = np.polyfit(pts[:,1], pts[:,2], 1)
		ys = np.polyval(fit, pts[:,1])
		rmse = np.sqrt(np.mean((ys-pts[:,2])**2))
		print(rmse)
		if rmse<32:
			invalid = False
		else:
			invalid = True

		# too short
		if len(pts) < 50:
			invalid = True


		if not invalid:
			pts[:,1] = np.clip(pts[:,1],0,w)
			pts[:,2] = np.clip(pts[:,2],0,h)
			valid_tracks.append(pts)
	return valid_tracks

def get_velocity(tracks):
	trails = []
	smoothing = scipy.signal.get_window('hann', 30)
	smoothing = smoothing / np.sum(smoothing)

	for track in tracks:
		a,b = track.shape
		p = np.zeros((a,b+4))
		p[:,0:5] = track
		p[:,5] = np.gradient(track[:,1])
		p[:,6] = np.gradient(track[:,2])
		p[:,7] = np.sqrt(p[:,5]**2+p[:,6]**2)

		p[:,7] = scipy.signal.convolve(p[:,7], smoothing, mode='same')
		p[:,8] = np.arctan2(p[:,1], p[:,2])

		trails.append(p)
	return trails

def draw_trails(tracks, background=None, speed_range=None, colour=(0, 0, 1), spline=True, line_width=0.5, draw_a=1):
	pylab.subplot(2,1,1)

	num_trails = 0
	if background is not None:
		pylab.imshow(background)
	for track in tracks:
		mean_speed = np.mean(track[:,6])
		mean_y = np.mean(track[:,2])

		interaction = False
		use_trail = True
		if not(not speed_range or (speed_range[0]<mean_speed<speed_range[1])):
			use_trail = False

		if spline:
			x = track[:,1]
			y = track[:,2]
			t = np.arange(len(track))

			spx = scipy.interpolate.UnivariateSpline(t,x,s=1e4,k=4)
			spy = scipy.interpolate.UnivariateSpline(t,y,s=1e4,k=4)
			spline_x = spx(t)
			spline_y = spy(t)
		else:
			spline_x = track[:,1]
			spline_y = track[:,2]


		if use_trail:

			num_trails += 1
			interaction = np.any(track[:,4])
			print(interaction)
			if interaction:
				pylab.plot(spline_x, spline_y, alpha=draw_a, c=colour, lw=line_width, label="Interactive")
			else:
				pylab.plot(spline_x, spline_y, alpha=draw_a, c="r", lw=line_width, label="Passive")

	pylab.ylim(h-border,border)
	pylab.xlim(border,w-border)



	pylab.axis('off')
	pylab.subplot(2,1,2)
	speeds = [np.mean(track[:,7]) for track in tracks]
	pylab.hist(speeds, bins=20)
	pylab.xlabel("SPEED")
	pylab.ylabel("No Of People")
	if speed_range:
		pylab.axvline(speed_range[0],c='r')
		pylab.axvline(speed_range[1],c='r')


	pylab.figure()
	interaction_times = [np.sum(track[:, 4]) for track in tracks]
	pylab.hist(interaction_times)
	pylab.xlabel("User")
	pylab.ylabel("Time in Frames")

	print(num_trails)
	return num_trails


def plot_trails(csvfile, backgroundfile, graph):

	background = pylab.imread(backgroundfile)

	trace = np.loadtxt(csvfile, comments=';', delimiter=',')
	traces = itertools.groupby(trace, lambda x:x[0])


	if graph == "all":
		valid_trails = get_velocity(all_tracks(traces))
		pylab.figure()
		draw_trails(valid_trails, background=background, colour=(1, 1, 0), line_width=2, draw_a=.2)

	elif graph == "filtered":
		valid_trails = get_velocity(filtered_tracks(traces))
		pylab.figure()
		draw_trails(valid_trails, background=background, colour=(1, 1, 0), line_width=2, draw_a=.2)

	pylab.show()

if __name__=="__main__":

	background = '../../media/bp.jpg'
	tracks = './demo_traces.csv'
	graph = "all"
	try:
		opts,args = getopt.getopt(sys.argv[1:], "f:b:g:")
	except getopt.GetoptError:
		print("Incorrect syntax")
		exit(2)

	for opt, arg in opts:
		if opt == "-f":
			tracks = arg
		elif opt == "-b":
			background = arg
		elif opt == "-g":
			graph = arg


	plot_trails(tracks, background,graph)
