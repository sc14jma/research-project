import config

def write_traces(traces, file_name):
	"""
	This function writes CSV data for each trace in the format PedestrianID , X, Y, FrameNumber;
	"""

	trace_f = open(file_name + "_traces.csv", "w")
	for id in traces:
		for set in traces[id]:
			thing = "" + str(id) + "," + str(set[0]) + "," + str(set[1]) + "," + str(set[2]) + "," + str(set[3])+ ";\n"
			trace_f.write(thing)
	trace_f.close()
