import ast
import polyROISelector
import yaml
import numpy as np
import cv2
import pickle

def select_roi(frame):
	ROISelector = polyROISelector.orientedROISelector(frame)
	while 1:
		k = cv2.waitKey(1)

		if k == ord('r'):
			return ROISelector.ROIs[0]['Polygon']
		elif k == (27):
			break
	cv2.destroyAllWindows()
	return None



def get_poly(filename):
	with open(filename, "r") as f:
		s = f.read()
		pts = ast.literal_eval(s)

	pts = yaml.load(pts)

	box = np.array(pts['Polygon'])
	return box

def inside_poly(x,y,points):
	for pts in points:
		n = len(pts)
		inside = False
		p1x, p1y = pts[0]
		for i in range(1, n + 1):
			p2x, p2y = pts[i % n]
			if y > min(p1y, p2y):
				if y <= max(p1y, p2y):
					if x <= max(p1x, p2x):
						if p1y != p2y:
							xinters = (y - p1y) * (p2x - p1x) / (p2y - p1y) + p1x
						if p1x == p2x or x <= xinters:
							inside = not inside
			p1x, p1y = p2x, p2y

		if inside == True:
			return 1
		else:
			return 0

def save(points):
	points = points.tolist()
	with open("points.txt", "w") as f:
		pickle.dump(points,f)
