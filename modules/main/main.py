

#  Please ensure the following dependencies are installed before use:
from __future__ import division
import imutils
import cv2
import sys, getopt
import blobs
import time
from poly_functions import *
from log_functions import *
from video_functions import *
import config
import time



def run(argv):
	"""
	Main Function for all video processing.  Defaults for this file are adjusted here.
	"""
	tracker = blobs.BlobTracker()


	selection = False
	#  Default Options for Running in Demo Mode
	video = "../../media/demo.mov"
	background = "../../media/bp.jpg"
	output =  "blob"
	method = "mog"
	file_name_base = "../../media/"
	results_base = "../../results/"

	try:
		opts,args = getopt.getopt(argv, "v:b:o:m:r:")
	except getopt.GetoptError:
		print "Getopt Error"
		exit(2)

	for opt, arg in opts:
		if opt == "-v":
			if arg == "0":
				video = 0
				selection = True
			elif arg == "1":
				video = 1
				selection = True
			else:
				video = arg
		elif opt == "-b":
			background = arg

		elif opt =="-r":
			if arg == "True":
				selection = True
			else:
				selection = False

		elif opt == "-m":
			method = arg




	print video , " " , background , " ", method


	file_name_base = "../../results/" + str(video) + "_"
	moments = "moments.txt"
	c = cv2.VideoCapture(video)






	c.set(0, 000.0)


	c.set(3,config.width)
	c.set(4,config.height)
	fps = c.get(5)
	fourcc = c.get(6)
	frames = c.get(7)

	#  Print out some initial information about the video to be processed.
	# print fourcc, fps, config.width, config.height, frames
	ret, f = c.read()

	f = imutils.resize(f, width=config.width)


	if method == "mog":
	#  Setup MOG element for generated background subtractions
		bgs = cv2.createBackgroundSubtractorMOG2()

	# MOG Erosion.Dilation
		for_er = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (config.mog_er_w, config.mog_er_h))
		for_di = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (config.mog_di_w, config.mog_di_h))
	elif method == "ext":
		c_zero = cv2.imread(background)

		for_er = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (config.er_w, config.er_h))
		for_di = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (config.di_w, config.di_h))

	trails = np.zeros((config.height, config.width, 3)).astype(np.uint8)
	start_t = time.clock()
	current_frame = 0

	if selection == True:
		poly_points = select_roi(f)
		save(poly_points)
		if poly_points is None:
			poly_points = get_poly(moments)
	else:
		poly_points = get_poly(moments)


	while 1:

		#  Get the next frame of the video
		ret,f = c.read()

		f = imutils.resize(f, width=400)


		f = cv2.GaussianBlur(f, (5, 5), 0)
		orig = f.copy()
		# grey_image = bgs.apply(f)
		print "current_frame", current_frame
		current_frame += 1


		if method == "mog":

			grey_image = bgs.apply(f)

			#  Turn this into a black and white image (white is movement)
			thresh, im_bw = cv2.threshold(grey_image, 225, 255, cv2.THRESH_BINARY)

		else:

			# im_zero = cv2.convertScaleAbs(c_zero)
			im_zero = c_zero.astype(np.uint8)

			#cv2.imshow("Im_zero", im_zero)

			#  Get the absolute diff
			d1 = cv2.absdiff(f, im_zero)

			#  Convert this to greyscale
			gray_image = cv2.cvtColor(d1, cv2.COLOR_BGR2GRAY)


			#  Turn this into a black and white image (white is movement)
			thresh, im_bw = cv2.threshold(gray_image, 15, 255, cv2.THRESH_BINARY)

		cv2.imshow("Binary Image", im_bw)



		#  Erode and Dilate Image to make blobs clearer.  Adjust erosion and dilation values in config
		im_er = cv2.erode(im_bw, for_er)
		im_dl = cv2.dilate(im_er, for_di)



		cv2.imshow("Eroded/Dilated Image", im_dl)

		_,contours, hierarchy = cv2.findContours(im_dl, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

		my_blobs = []
		counter = 0
		for cnt in contours:
			try:

				size = cv2.contourArea(cnt)

				if size >250:
					x0,y0,w,h = cv2.boundingRect(cnt)



					moments = cv2.moments(cnt)
					x = int(moments['m10'] / moments['m00'])
					y = int(moments['m01'] / moments['m00'])

					inside = inside_poly(x,y,poly_points)
					if inside:
						cv2.rectangle(f, (x0, y0), (x0 + w, y0 + h), (255, 0, 0), 2)
					else:
						cv2.rectangle(f, (x0, y0), (x0 + w, y0 + h), (255, 255, 0), 2)

					my_blobs.append((x, y, inside))


			except cv2.error as e:
				print e

		if len(my_blobs) > 0:
			tracker.track_blobs(my_blobs, [0,0,config.width,config.height], current_frame)

			for v in tracker.virtual_blobs:
				size = 5
				if v.got_updated:
					size = 10
				cv2.rectangle(f, (int(v.x),int(v.y)), (int(v.x+size), int(v.y+size)), v.color, size)



		if config.draw_video:

			v = len(tracker.virtual_blobs)
			font = cv2.FONT_HERSHEY_SIMPLEX
			cv2.putText(f, "Active Users: %d" %(v), (25, 25), font,0.5, (0, 255, 0), 1, cv2.LINE_AA)
			# counter += v
			# cv2.putText(f, "Total Users: %d" % (counter), (25, 35), font, 0.5, (0, 255, 0), 1, cv2.LINE_AA)

			for id in tracker.traces:

				ox = None
				oy = None


				if len(tracker.traces[id])>2:
					for pos in tracker.traces[id][-3:]:

						x = int(pos[0])
						y = int(pos[1])

						if ox and oy:
							sx = int(0.8*ox + 0.2*x)
							sy = int(0.8*oy + 0.2*y)

							#  Colours are BGRA
							cv2.line(trails, (sx,sy), (ox, oy), (0,128,255), 1)
							font = cv2.FONT_HERSHEY_SIMPLEX
							# # cv2.putText(f, "ID: %d -> %s" % (id, (x,y)), (10, (id + 1) * 25), font,
							# 			0.6, (0, 255, 0), 1, cv2.LINE_AA)

							oy = sy
							ox = sx
						else:
							ox,oy = x,y

			try:
				cv2.add(f,trails,f)
			except cv2.error as e:
				print len(trails)
				print e

			cv2.drawContours(f,contours,-1,(0,255,0),1)

			#  draw frame
			cv2.rectangle(f, (config.FRAME_WIDTH, config.FRAME_WIDTH), (config.width - config.FRAME_WIDTH, config.height - config.FRAME_WIDTH), (0, 0, 0), 2)


			cv2.imshow('output',f)






		if frames == current_frame:

			#  Save the pic and traces
			t = str(time.time())
			cv2.imwrite(results_base + t + "_last_frame.png", orig)
			write_traces(tracker.traces, results_base + t)


			break

		k = cv2.waitKey(25)
		if k == 27:
			t= str(time.time())
			print "writing traces"
			cv2.imwrite(results_base+t+"_last_frame.png", orig)
			write_traces(tracker.traces, results_base+t )
			break


	c.release()
	cv2.destroyAllWindows()




if __name__ == "__main__":
	run(sys.argv[1:])
